using Xunit;

namespace MyHelloWorld.UnitTest
{
    public class HelloWorldBuilderTest
    {
        [Fact]
        public void ShouldBeHelloWorldOnBuild()
        {
            Assert.Equal("Hello World", HelloWorldBuilder.Build());
        }
    }
}
