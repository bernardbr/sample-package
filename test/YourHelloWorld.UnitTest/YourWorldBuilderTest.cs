using System;
using Xunit;

namespace YourHelloWorld.UnitTest
{
    public class YourWorldBuilderTest
    {
        [Fact]
        public void ShouldBeYourHelloWorldOnBuild()
        {
            Assert.Equal("Your Hello World", YourHelloWorldBuilder.Build());
        }
    }
}
