﻿namespace MyHelloWorld
{
    public static class HelloWorldBuilder
    {
        public static string Build()
        {
            return "Hello World";
        }
    }
}
